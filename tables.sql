
drop index MARCHANDISE_PK;

drop index MARCHANDISE_FK1;

drop index MARCHANDISE_FK2;

drop table MARCHANDISE cascade constraints;

drop index CONTENU_PK;

drop index CONTENU_FK1;

drop index CONTENU_FK2;

drop table CONTENU cascade constraints;

drop index ASSIGNATION_PK;

drop index ASSIGNATION_FK1;

drop index ASSIGNATION_FK2;

drop table ASSIGNATION cascade constraints;

drop index PRODUIT_PK;

drop index PRODUIT_FK1;

drop index PRODUIT_FK2;

drop table PRODUIT cascade constraints;

drop index LIVRAISON_PK;

drop index LIVRAISON_FK1;

drop index LIVRAISON_FK2;

drop index LIVRAISON_FK3;

drop index LIVRAISON_FK4;

drop table LIVRAISON cascade constraints;

drop index DEPOT_PK;

drop table DEPOT cascade constraints;

drop index CAMION_PK;

drop table CAMION cascade constraints;

drop index CHAUFFEUR_PK;

drop table CHAUFFEUR cascade constraints;


create table CHAUFFEUR
(
	NUMERO_CHAUFFEUR		INT(4)			not null,
	NOM_CHAUFFEUR 			CHAR(30)		not null,
	PRENOM_CHAUFFEUR		CHAR(30)		not null,
	ADRESSE_CHAUFFEUR		CHAR(50)		not null,
	VILLE_CHAUFFEUR			CHAR(30)		not null,
	ANCIENNETE_CHAUFFEUR		INT(2)			not null,
	TYPE_DE_PERMIS			CHAR(3)			not null,
	ABSENCES			INT(3)			        ,
	RESPECT_ROUTE			INT(2)				,
	constraint pk_chauffeur primary key (NUMERO_CHAUFFEUR)
);


create table CAMION
(
	IMMATRICULATION			CHAR(7)			not null,
	MARQUE_CAMION			CHAR(20)		not null,
	TYPE_CAMION			CHAR(30)		not null,
	MODELE_CAMION			CHAR(30)		not null,
	DATE_MISE_EN_SERVICE		DATE			        ,
	DATE_ACHAT			DATE				,
	LATITUDE_CAMION			FLOAT(10)		not null,
	LONGITUDE_CAMION		FLOAT(10)		not null,
	KILOMETRAGE			INT(6)				,
	CAPACITE			INT(6)			not null,
	ETAT				CHAR(10)		not null,
	constraint pk_camion primary key (IMMATRICULATION)
);

create table DEPOT
(
	NUMERO_DEPOT			INT(4)			not null,
	INTITULE_DEPOT			CHAR(30)		not null,
	LONGITUDE_DEPOT			FLOAT(10)		not null,
	LATITUDE_DEPOT			FLOAT(10)		not null,
	VILLE_DEPOT			CHAR(30)		not null,
	constraint pk_depot primary key (NUMERO_DEPOT)
);

create table LIVRAISON
(
	NUMERO_LIVRAISON		INT(8)			not null,
	KM_LIVRAISON			INT(5)			not null,
	DATE_LIVRAISON			DATE			not null,	,
	NUMERO_CHAUFFEUR 		INT(4)			not null,
	IMMATRICULATION			CHAR(7)			not null,
	NUMERO_DEPOT_SRC		INT(4)			not null,
	NUMERO_DEPOT_DEST		INT(4)			not null,
	constraint pk_livraison primary key (NUMERO_LIVRAISON)
);

create index LIVRAISON_FK1 on LIVRAISON (NUMERO_CHAUFFEUR asc);
create index LIVRAISON_FK2 on LIVRAISON (IMMATRICULATION asc);
create index LIVRAISON_FK3 on LIVRAISON (NUMERO_DEPOT_SRC asc);
create index LIVRAISON_FK4 on LIVRAISON (NUMERO_DEPOT_DEST asc);

create table PRODUIT
(
	NUMERO_PRODUIT			INT(8)		not null,
	POIDS_PRODUIT			INT(5)		not null,
	constraint pk_produit primary key (NUMERO_PRODUIT)
);

create table ASSIGNATION
(
	NUMERO_CHAUFFEUR		INT(4)			not null,
	IMMATRICULATION			CHAR(7)			not null,
	constraint pk_assignation primary key (NUMERO_CHAUFFEUR, IMMATRICULATION)
);
create index ASSIGNATION_FK1 on ASSIGNATION (NUMERO_CHAUFFEUR asc);
create index ASSIGNATION_FK2 on ASSIGNATION (IMMATRICULATION asc);

create table STOCK
(
	NUMERO_DEPOT			INT(4)		not null,
	NUMERO_PRODUIT			INT(8)		not null,
	QUANTITE_DISPONIBLE		INT(5)			,
	constraint pk_contenu primary key (NUMERO_DEPOT, NUMERO_PRODUIT)
);
create index STOCK_FK1 on STOCK  (NUMERO_DEPOT asc);
create index STOCK_FK2 on STOCK  (NUMERO_PRODUIT asc);

create table MARCHANDISE
(
	NUMERO_LIVRAISON		INT(8)		not null,	
	NUMERO_PRODUIT			INT(8)		not null,
	QUANTITE			INT(5)			,
	constraint pk_marchandise primary key (NUMERO_LIVRAISON, NUMERO_PRODUIT)
);

create index MARCHANDISE_FK1 on MARCHANDISE (NUMERO_LIVRAISON asc);
create index MARCHANDISE_FK2 on MARCHANDISE (NUMERO_PRODUIT asc);

alter table ASSIGNATION
    add constraint fk1_assignation foreign key (NUMERO_CHAUFFEUR)
       references CHAUFFEUR (NUMERO_CHAUFFEUR);

alter table ASSIGNATION
    add constraint fk2_assignation foreign key (IMMATRICULATION)
       references CAMION (IMMATRICULATION);

alter table LIVRAISON
    add constraint fk1_livraison foreign key (NUMERO_CHAUFFEUR)
       references CHAUFFEUR (NUMERO_CHAUFFEUR);

alter table LIVRAISON
    add constraint fk2_livraison foreign key (IMMATRICULATION)
       references CAMION (IMMATRICULATION);

alter table LIVRAISON
    add constraint fk3_livraison foreign key (NUMERO_DEPOT_SRC)
       references DEPOT (NUMERO_DEPOT);

alter table LIVRAISON
    add constraint fk4_livraison foreign key (NUMERO_DEPOT_DEST)
       references DEPOT (NUMERO_DEPOT);

alter table MARCHANDISE
    add constraint fk1_marchandise foreign key (NUMERO_LIVRAISON)
       references LIVRAISON (NUMERO_LIVRAISON);

alter table MARCHANDISE
    add constraint fk2_marchandise foreign key (NUMERO_PRODUIT)
       references PRODUIT (NUMERO_PRODUIT);

alter table STOCK
    add constraint fk1_contenu foreign key (NUMERO_DEPOT)
       references DEPOT (NUMERO_DEPOT);

alter table STOCK
    add constraint fk2_contenu foreign key (NUMERO_PRODUIT)
       references PRODUIT (NUMERO_PRODUIT);
