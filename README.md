Ce projet a été effectué conjointement par Hind Boucherit, Eléonore Coutolleau et Sonia Ouedraogo

Manuel d'utilisation

Pour faire fonctionner les requêtes sur la base de données de PHPMyAdmin, veuillez
mettre tous les fichiers dans votre serveur public pour assurer le bon fonctionnement
du projet.

Une fois ceci fait, lancez votre page le fichier index.html lancera la page d'accueil
automatiquement. La page d'accueil contient ainsi tous les éléments nécessaires à la
gestion de la base e données.

Concernant les requêtes de consulation à écrire manuellement, vous n'avez la possibilité
de n'obtenir des informations que sur un attribut, il n'est pas possible d'écrire une requête
du type  select NUMERO_CHAUFFEUR, PRENOM_CHAUFFEUR from CHAUFFEUR;

Les requêtes de la section consultation sont celles demandées dans le sujet. Attention
à bien respecter les formats des données rentrées : date aa-mm-dd pour obtenir la liste
des livraisons, ville en majuscule pour les chauffeurs et numero de dépôt en numérique
pour la liste des camions.

Pour les requêtes statistiques, il suffit de cliquer sur le bouton correspondant à l'opération
voulue excepté pour les classements qui nécessites de votre part des données à rentrer selon la
position que vous voulez atteindre ou le dépôt visé.

En vous souhaitant une bonne expérience.